# BufferOverflow #2

> 200 points
>
> The close cousin of a website for "Question marked as duplicate" - part 3!
>
> Can you control the arguments to `win()` and get the flag?

Arguments sit on the stack underneath the return pointer, so we just keep
overwriting and put our arguments there. Remember to make space for `win()`'s
return pointer, and make sure the first argument is 64 bits long---`long long`
is 64 bits wide.
