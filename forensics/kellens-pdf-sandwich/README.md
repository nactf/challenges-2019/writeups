>Kellen was playing some more World of Tanks....

>He played so much WOT that he worked up an appetite.

>Kellen ripped a PDF in half. He then treated these two halves as bread and placed a different PDF on the inside (yummy PDF meat!). That sounds like one good PDF sandwich. PDF on the outside and inside! YUM!

If we open up the file we are given we see a corrupted file. This is because one PDF is literally inside another PDF.


But, if we place the file into a hex editor we see that there are 2 PDF headers and 2 EOFs. The "meat" PDF will start at the 2nd header and end at the 1st EOF. The 2nd header starts at 0x00DC and the 1st EOF ends at 0x3DBB.
We then extract the "meat" and it shows us the second half of the flag! The remaining PDF (the bread) contains the first half of the flag.

When we concatenate these two halves we get the flag: nactf{w3_l0v3_w0rld_0f_t4nk5ejwjfae}