>Joyce's friend just sent her this photo, but it's really fuzzy. She has no idea what the message says but she thinks she can make out some black text in the middle. She gave the photo to Oligar, but even his super eyes couldn't read the text. Maybe you can write some code to find the message?

>Also, you might have to look at your screen from an angle to see the blurry hidden text

>P.S. Joyce's friend said that part of the message is hidden in every 6th pixel

This problem was a little bit confusing... By every 6th pixel, the challenge creator meant that a pixel that was in every 6th row and every 6th collumn was part of the original flag image.

Lets write a python script to take the original image and extract every 6ith pixel:

```
from PIL import Image
import random
im = Image.open('purple.png') // I renamed the file name
pixelMap = im.load()


img = Image.new( im.mode, im.size)
pixelsNew = img.load()

for i in range(img.size[0]):
    for j in range(img.size[1]):
        if j%6 == 0 and i%6 == 0:
            pixelsNew[i/6,j/6] = pixelMap[i,j]

img.show()
```



This shows us the flag: nactf{fu22y_boy5_un1t3} (I think the challenge creator forgot to add in the last bracket but we can assume its there)
