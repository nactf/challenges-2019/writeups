from PIL import Image
import random
im = Image.open('purple.png')
pixelMap = im.load()


img = Image.new( im.mode, im.size)
pixelsNew = img.load()

for i in range(img.size[0]):
    for j in range(img.size[1]):
        if j%6 == 0 and i%6 == 0:
            pixelsNew[i/6,j/6] = pixelMap[i,j]

img.show()

