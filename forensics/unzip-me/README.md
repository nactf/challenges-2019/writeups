>I stole these files off of The20thDucks' computer, but it seems he was smart enough to put a password on them. Can you unzip them for me?

Ahh.. so these are zip files protects with passwords. I personally like using fcrackzip to crack these and I use rockyou as my dictionary. This process should be pretty easy, I just give fcrackzip rockyou and the zip file and it will find the password for me.

I use this command: `` fcrackzip -v -u -D -p rockyou-75.txt zip1.zip``

And I get this response: PASSWORD FOUND!!!!: pw == dictionary

If we do this for the other 2 zips we see that their passwords are rock and dog.

When we open the files, each one of them has a piece of the flag. Then, we can concatenate these files we get the flag: nactf{w0w_y0u_unz1pp3d_m3}