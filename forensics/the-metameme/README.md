>Phil sent me this meme and its a little but suspicious. The meme is super meta and it may be even more meta than you think.

>Wouldn't it be really cool if it also had a flag hidden somewhere in it? Well you are in luck because it certainly does!

The problem seems to be reffereing to the metadata of the file. There are some 
websites that can return the metadata of a file for us. For example, this website: https://www.get-metadata.com

If we give the website our metameme we can see that the subject of the file is :nactf{d4mn_th15_1s_s0_m3t4}