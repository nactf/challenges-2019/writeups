# Reversible Sneaky Algorithm #2

> Oligar was thinking about number theory at AwesomeMath when he decided to encrypt a message with RSA. As a mathematician, he made various observations about the numbers. He told Molly one such observation:
>
> a^r ≡ 1 (mod n)
>
> He isn't SHOR if he accidentally revealed anything by telling Molly this fact... can you decrypt his message?
>
> Source code, a and r, public key, and ciphertext are attached.

This problem gives us a message encrypted with RSA. The public modulus is too large to be factored, so it seems impossible to decrypt. 
However, we look up and find that Shor's Algorithm allows quantum computers to quickly factor large numbers.
In Shor's Algorithm, the quantum part allows us to find the multiplicative order _r_ of a number _a_ modulo _n_ so that
```
a^r ≡ 1 (mod n)
```
Therefore,
```
a^r - 1 ≡ 0 (mod n)
```
And assuming _r_ is even, we can factor
```
(a^(r/2) + 1)(a^(r/2) - 1) = k*n

```
Since there exists a unique prime factorization of n, the factors _p_ and _q_ must divide either a^(r/2)-1 or a^(r/2)+1. If _p_ 
goes into one multiplicand and _q_ goes into the other, then we can calculate the greatest common divisor of _n_ and either of the multiplicands
to recover a factor of _n_. 

Now, the problem gives us _a_ and _r_, so we don't need a quantum computer. We follow the classical computer part of Shor's algorithm to factor _n_.
From there we can calculate the private key and decrypt the flag.

nactf{d0wn_wi7h_7h3_0lig4rchy}