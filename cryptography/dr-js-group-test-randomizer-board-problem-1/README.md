# Dr. J's Group Test Randomizer: Board Problem #1

> Dr. J is back with another group test, and he patched his prng so we can't predict the next number based on the previous one! Can still you help Leaf predict the next output of the prng?
>
> nc shell.2019.nactf.com 31258

This challenge is similar to Dr. J's Board Problem #0, except that only the digits 13-16 are given as output. 
Since we don't have the middle 8 digits, we cannot calculate the middle square and predict the next output.
However, each output is still uniquely determined by the middle 8 digits. Therefore, if those 8 digits are ever reached again 
the outputs will start to loop. 8 digits is not a large range -- it is a known weakness of the middle square method that outputs tend
 to loop after a short time. So we write a program to request outputs and keep a history. Once the numbers start to loop, we submit
 the next two numbers in our loop history and receive the flag.
 
nactf{th3_b35t_pr3d1ct0r_0f_fu7ur3_b3h4v10r_15_p4st_b3h4v10r}