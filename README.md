# NACTF 2019 Writeups

## Table of Contents

[General Skills](#general-skills)

[Web Exploitation](#web-exploitation)

[Cryptography](#cryptography)

[Forensics](#forensics)

[Binary Exploitation](#binary-exploitation)

[Reverse Engineering](#reverse-engineering)


## General Skills

## Web Exploitation

## Cryptography

50 pts - [Vyom's Soggy Croutons](cryptography/vyoms-soggy-croutons)

50 pts - [Loony Tunes](cryptography/loony-tunes)

100 pts - [Dr. J's Group Test Randomizer: Board Problem #0](cryptography/dr-js-group-test-randomizer-board-problem-0)

125 pts - [Reversible Sneaky Algorithm #0](cryptography/reversible-sneaky-algorithm-0)

250 pts - [Super Duper AES](cryptography/super-duper-aes)

275 pts - [Reversible Sneaky Algorithm #1](cryptography/reversible-sneaky-algorithm-1)

300 pts - [Dr. J's Group Test Randomizer: Board Problem #1](cryptography/dr-js-group-test-randomizer-board-problem-1)

350 pts - [Reversible Sneaky Algorithm #2](cryptography/reversible-sneaky-algorithm-2)

625 pts - [Dr. J's Group Test Randomizer #2: BBOB](cryptography/dr-js-group-test-randomizer-2-bbob)

## Forensics

50 pts - [Least Significant Avenger](forensics/least-significant-avenger)

75 pts - [The Metameme](forensics/the-metameme)

75 pts - [My Ears Hurt](forensics/my-ears-hurt)

150 pts - [Unzip Me](forensics/unzip-me)

150 pts - [Kellen's Broken File](forensics/kellens-broken-file)

150 pts - [Kellen's PDF Sandwich](forensics/kellens-pdf-sandwich)

250 pts - [Phuzzy Photo](forensics/phuzzy-photo)

## Binary Exploitation

100 pts - [BufferOverflow #0](binary/bufover-0/)

200 pts - [BufferOverflow #1](binary/bufover-1/)

200 pts - [BufferOverflow #2](binary/bufover-2/)

200 pts - [Format #0](binary/format-0/)

250 pts - [Format #1](binary/format-1/)

350 pts - [Loopy #0](binary/loopy-0/)

500 pts - [Loopy #1](binary/loopy-1/)


## Reverse Engineering

